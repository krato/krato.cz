FROM php:7.1-cli
WORKDIR /var/www/www
CMD [ "php", "-S", "0.0.0.0:80" ]
EXPOSE 80


<?php

namespace App\Controls;

class MenuControl extends \Nette\Application\UI\Control
{

	private $menuItems;

	public function __construct(array $menuItems, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
	{
		parent::__construct($parent, $name);
		$this->menuItems = $menuItems;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/default.latte');

		$this->template->items = $this->menuItems;

		$this->template->render();
	}	
}

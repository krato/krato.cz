<?php

namespace App\Presenters;

use Nette;


class BasePresenter extends Nette\Application\UI\Presenter
{
	/** @inject @var \App\Repositories\MenuRepository */
	public $menuRepository;
	
	public function createComponentMenu()
	{
		return new \App\Controls\MenuControl($this->menuRepository->menuItems);
	}
}

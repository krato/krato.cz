<?php

namespace App\Presenters;

use Nette;

class CVPresenter extends BasePresenter
{

	/** @inject @var \App\Repositories\CVRepository */
	public $cvRepository;

	public function renderDefault()
	{
		$this->template->cv = $this->cvRepository->cv;
		$this->template->cvHtml = $this->cvRepository->html;
	}

	public function createComponentEditForm()
	{
		$form = new \Nette\Application\UI\Form();
		$form->addTextArea('body', '', 156, 35);
		$form->addSubmit('cancel', 'Zrušit')->setAttribute('class', 'btn btn-danger');
		$form->addSubmit('submit', 'Uložit')->setAttribute('class', 'btn btn-success');
		$form->setDefaults([
			'body' => $this->cvRepository->html,
		]);
		$form->onSuccess[] = $this->formEditSuccess;
		return $form;
	}

	public function formEditSuccess(\Nette\Application\UI\Form $form, $values)
	{
		if ($form['submit']->isSubmittedBy())
		{
			if ($this->cvRepository->cv['changesAllowed'])
			{
				$this->cvRepository->html = $values->body;
				$this->flashMessage('Životopis je aktualizován.');
			} else
			{
				$form->addError('Changes are not allowed!');
				return;
			}
		}
		$this->redirect('CV:default');
	}

}

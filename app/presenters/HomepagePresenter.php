<?php

namespace App\Presenters;

use Nette;

class HomepagePresenter extends BasePresenter
{

	/** @inject @var \App\Repositories\CVRepository */
	public $cvRepository;

	public function renderDefault()
	{
		$this->template->cv = $this->cvRepository->cv;
	}

}

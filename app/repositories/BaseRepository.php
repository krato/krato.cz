<?php

namespace App\Repositories;

/**
 * @property-read string $tableName
 */
abstract class BaseRepository extends \Nette\Object
{

    /** @var string */
    protected $tableName;

    /** @var \Nette\Database\Context */
    protected $context;

    public function __construct($tableName, \Nette\Database\Context $context)
    {
        $this->tableName = $tableName;
        $this->context = $context;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function findAll()
    {
        return $this->context->table($this->tableName);
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function findById($id)
    {
        return $this->findAll()->where(['id' => $id]);
    }
    
    /**
     * Insert new row
     * @param array $values
     * @return \Nette\Database\Table\IRow the new row
     */
    public function insert($values)
    {
        return $this->findAll()->insert($values);
    }

}

<?php

namespace App\Repositories;

/**
 * @property-read array $cv CV configuration
 * @property string $html HTML version of Czech CV
 */
class CVRepository extends \Nette\Object
{

	protected $cv;

	public function __construct($cv)
	{
		$this->cv = $cv;
	}

	public function getCv()
	{
		return $this->cv;
	}

	protected function getHtmlFilePath()
	{
		return $_SERVER['DOCUMENT_ROOT'] . $this->cv['urls']['html'];
	}

	public function getHtml()
	{
		$filePath = $this->getHtmlFilePath();
		if (file_exists($filePath))
		{
			return file_get_contents($filePath);
		} else
		{
			return '';
		}
	}

	public function setHtml($html)
	{
		$filePath = $this->getHtmlFilePath();
		file_put_contents($filePath, $html);
	}

}

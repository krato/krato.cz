<?php

namespace App\Repositories;

/**
 * @property-read array $menuItems Menu items
 */
class MenuRepository extends \Nette\Object
{

	private $menuItems;

	public function __construct($menuItems)
	{
		$this->menuItems = $menuItems;
	}

	public function getMenuItems()
	{
		return $this->menuItems;
	}

}

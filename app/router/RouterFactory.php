<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter(array $routes)
	{
		$router = new RouteList;
		$routes['/cs/<presenter>/<action>[/<id>]'] = 'Homepage:default';
		foreach ($routes as $path => $action) {
			#$router[] = new Route('' . $path, $action, Route::SECURED);
			$router[] = new Route('' . $path, $action);
			if (substr($path, 0, 4) == '/cs/')
			{
				$router[] = new Route('/' . substr($path, 4), $action, Route::SECURED);
			}
		}
		return $router;
	}

}

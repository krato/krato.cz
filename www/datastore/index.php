<?php

$db = new PDO('mysql:dbname=c_datastore;host=127.0.0.1', 'c_datastore', 'c_datastore');

$op = $_REQUEST['op'];
$key = $_REQUEST['key'];

$data = isset($_REQUEST['data']) ? $_REQUEST['data'] : null;
//$data = stream_get_contents(STDIN);

//var_dump($data);

switch ($op)
{
	case 'ver':
		$st = $db->prepare("SELECT `version` FROM `data` WHERE `key` = ?");
		$st->execute([$key]);
		$response = $st->fetch();
		echo($response['version']);
		break;
	case 'update':
		$st = $db->prepare("UPDATE `data` SET `data` = ?, `version` = `version` + 1 WHERE `key` = ?");
		$res = $st->execute([$data, $key]);
		echo($res ? 1 : 0);
		break;
	case 'get':
		$st = $db->prepare("SELECT `data` FROM `data` WHERE `key` = ?");
		$st->execute([$key]);
		$response = $st->fetch();
		echo($response['data']);
		break;
	case 'ping':
		echo 'pong';
		break;
}


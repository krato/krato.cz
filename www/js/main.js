/* Easy confirm based on: http://stackoverflow.com/questions/9139075/confirm-message-before-delete */
$(document).ready( function() {
    $('.delete').click(function () {
        return confirm("Are you sure to delete?");
    });
});
